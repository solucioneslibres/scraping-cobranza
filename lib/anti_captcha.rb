# frozen_string_literal: true

require 'net/http'

# Solve catchas with the anti-captcha.com API.
class AntiCaptcha
  ENDPOINT = URI('https://api.anti-captcha.com')

  def initialize(api_key, polling_interval = 10)
    @api_key = api_key
    @polling_interval = polling_interval
  end

  def create_task(task)
    request('createTask', { task: })
  end

  def get_task_result(task_id)
    result = nil
    loop do
      sleep(@polling_interval)
      result = request('getTaskResult', { taskId: task_id })
      break unless result[:status] == 'processing'
    end
    result
  end

  def report_correct_recaptcha(task_id)
    request('reportCorrectRecaptcha', { taskId: task_id })
  end

  def report_incorrect_recaptcha(task_id)
    request('reportIncorrectRecaptcha', { taskId: task_id })
  end

  protected

  # Makes a request against ENDPOINT/path. Appends clientKey to body.
  # @return [Hash] Parsed response with symbolized names.
  def request(path, body)
    Net::HTTP.start(
      ENDPOINT.host,
      ENDPOINT.port,
      use_ssl: ENDPOINT.scheme == 'https'
    ) do |http|
      request = Net::HTTP::Post.new(
        "#{ENDPOINT}/#{path}",
        {
          'Accept' => 'application/json',
          'Content-Type' => 'application/json'
        }
      )
      request.body = body.merge({ clientKey: @api_key }).to_json
      response = http.request(request)
      JSON.parse(response.body, symbolize_names: true)
    end
  end
end
