# Scraping cobranza

## Instalación

En un sistema GNU/Linux.

1. Instala [firefox](https://www.mozilla.org/firefox/).
2. Ejecuta `bundle install`.
3. Copia el archivo `credentials.example.rb` a `credentials.rb` y editalo con credenciales válidas.

## Uso

```
[ANTI_CAPTCHA_API_KEY=X] bundle exec bin/pi-scraper <boletas|gastos_comunes>
```

La extracción de datos de Edipro requiere la variable de entorno `[ANTI_CAPTCHA_API_KEY=X]`.

### Ayuda

```
bundle exec bin/pi-scraper help
```
