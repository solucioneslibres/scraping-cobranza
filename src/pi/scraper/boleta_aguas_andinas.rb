# frozen_string_literal: true

require 'csv'
require 'date'
require 'monkeys/pdf_reader'
require 'pi/scraper'
require 'pi/scraper/boleta_base'
require 'pi/scraper/exceptions/invalid_id_error'

module PI
  module Scraper
    # Downloads boleta and extracts data for an Aguas Andinas account.
    #
    # All is stored on storage/.
    #
    # == Example
    #   PI::Scraper::BoletaAguasAndinas
    #     .new(id_prop, nro_cuenta)
    #     .process(browser)
    class BoletaAguasAndinas < BoletaBase
      include Path
      include MoneyToInt

      ABBR_MONTHNAMES_ES =
        %w[ENE FEB MAR ABR MAY JUN JUL AGO SEP OCT NOV DIC].freeze

      ATTRS = %w[
        id_prop nro_cuenta lectura_anterior saldo_anterior meses_deuda
        lectura_actual total_a_pagar
      ].freeze

      # Automate browser to download PDF.
      # @param [Watir::Browser] browser Browser instance to use.
      def collect(browser)
        browser.goto(
          'https://www.aguasandinas.cl/web/aguasandinas/descarga-tu-boleta'
        )

        browser.radio(id: 'busqueda_n_cuenta').select
        numero_cuenta_field = browser.text_field(id: 'numeroCuenta')
        numero_cuenta_field.set(@nro_cuenta)
        browser.send_keys(:enter)
        numero_cuenta_field.wait_while(&:present?)

        if browser.div(id: 'listaResuldatos').div(id: 'respuesta_error')
            .present?
          warn(
            "Cuenta '#{@nro_cuenta}' no existe en Aguas Andinas (#{@id_prop})"
          )
          return
        end

        browser.radio(id: 'numeroCuenta').select
        browser.button(id: 'buscar_boletas').click
        browser.td(data_th: 'PDF').link.click
        move_download

        puts("aandinas: #{@nro_cuenta} (#{@id_prop}) - OK")
        true
      end

      # Extract data from PDF.
      def extract
        text = PDF::Reader.new(@pdf_path).pages.first.text(unformatted: true)

        match = text.match(/LECTURA ANTERIOR\s+(\d+)-([A-Z]+)-(\d+)\s/)
        @lectura_anterior = match ? date_match_to_date(match) : nil

        match = text.match(/SALDO ANTERIOR\s+\((\d+)\)\s+([\d.]+)\s/)
        if match
          @saldo_anterior = money_to_int(match[2])
          @meses_deuda = match[1].to_i
        else
          @saldo_anterior = @meses_deuda = 0
        end

        @lectura_actual = date_match_to_date(
          text.match(/LECTURA ACTUAL\s+(\d+)-([A-Z]+)-(\d+)\s/)
        )
        @total_a_pagar = money_to_int(
          text.match(/TOTAL A PAGAR\s+\$\s([\d.]+)/)[1]
        )
      end

      protected

      def nro_cuenta=(value)
        @nro_cuenta = value&.strip&.split('-')&.[](0)
        return unless @nro_cuenta&.length != 7

        raise(
          InvalidIdError,
          "Cuenta '#{@nro_cuenta}' inválida para Aguas Andinas (#{@id_prop})"
        )
      end

      def date_match_to_date(match)
        Date.new(
          match[3].to_i,
          ABBR_MONTHNAMES_ES.find_index(match[2]) + 1,
          match[1].to_i
        )
      end
    end
  end
end
