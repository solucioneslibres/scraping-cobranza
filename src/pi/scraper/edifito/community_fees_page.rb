# frozen_string_literal: true

require 'date'
require 'pi/scraper'

module PI
  module Scraper
    class Edifito
      # Community fees page for Edifito.
      #
      # Extracts: mes_gc, monto_gc, total_deuda, fecha_ult_pago, monto_ult_pago.
      class CommunityFeesPage
        include MoneyToInt

        URL = 'https://clientes.edifito.com/main.php?prm0=0x2200'

        def initialize(browser)
          @browser = browser
        end

        def open(year = nil, month = nil)
          if [year, month].include?(nil)
            @browser.goto(URL)
          else
            @browser.goto("#{URL}&anio=#{year}&mes=#{month}")
          end

          @browser.execute_script('mostrarColilla()')
          @browser.wait_until{ loaded? }
          self
        end

        def loaded?
          h1.text == 'Colilla de Cobro'
        end

        def extract
          mes_gc = date_str_to_month(until_date_cell.text)
          monto_gc = money_to_int(month_amount_cell.text)
          total_deuda = money_to_int(debt_amount_cell.text)

          while payments_table.nil?
            open_prev_month
            ((fecha_ult_pago = monto_ult_pago = '') && break) if
              until_date_cell.text == ''
          end

          fecha_ult_pago ||= date_str_to_month(last_payment_date_cell.text)
          monto_ult_pago ||= money_to_int(last_payment_amount_cell.text)

          { mes_gc:, monto_gc:, total_deuda:, fecha_ult_pago:, monto_ult_pago: }
        end

        def logout
          @browser.goto('https://clientes.edifito.com/SesionDesconectar.php')
        end

        protected

        def str_to_date(value)
          Date.strptime(value, '%d-%m-%Y')
        end

        def date_to_month_str(date)
          date.strftime('%Y-%m')
        end

        def date_str_to_month(value)
          date_to_month_str(str_to_date(value))
        end

        def open_prev_month
          date = str_to_date(until_date_cell.text)
          if date.month == 1
            year = date.year - 1
            month = 12
          else
            year = date.year
            month = date.month - 1
          end
          self.open(year, month)
        end

        def h1 = @browser.h1
        def unit_table = @browser.table(index: 0)
        def unit_name_cell = unit_table.tbody.row(index: 0).cell(index: 1)
        def info_table = @browser.table(index: 1)
        def until_date_cell = info_table.row(index: 3).cell(index: 1)

        def month_amount_cell
          @browser.tfoot(index: -1).row(index: 0).cell(index: -1)
        end

        def payments_table
          table = @browser.table(index: -1)
          return nil unless
            table&.row(index: 0)&.cell(index: 0)&.text == 'Comprobante'

          table
        end

        def debt_amount_cell
          return payments_table.tfoot.row(index: 0).cell(index: 1) unless
            payments_table.nil?

          @browser.tfoot(index: -1).row(index: -1).cell(index: -1)
        end

        def last_payment_date_cell
          payments_table.tbody.row(index: -2).cell(index: 1)
        end

        def last_payment_amount_cell
          payments_table.tbody.row(index: -2).cell(index: 2)
        end
      end
    end
  end
end
