# frozen_string_literal: true

require 'pi/scraper/edifito/select_property_page'
require 'watir/wait'

module PI
  module Scraper
    class Edifito
      # Login page for Edifito.
      class LoginPage
        URL = 'https://clientes.edifito.com/main.php?prm0=0x0010'

        def initialize(browser)
          @browser = browser
        end

        def open
          @browser.goto(URL)
          self
        end

        def login(user, password)
          login_field.set(user)
          password_field.set(password)
          @browser.send_keys(:enter)
          next_page = SelectPropertyPage.new(@browser)
          Watir::Wait.until { next_page.loaded? }
        rescue Watir::Wait::TimeoutError
          login(user, password)
        end

        protected

        def login_field = @browser.text_field(id: 'ClienteLogin')
        def password_field = @browser.text_field(id: 'ClientePassword')
      end
    end
  end
end
