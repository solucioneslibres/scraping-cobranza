# frozen_string_literal: true

module PI
  module Scraper
    class Edifito
      # Select property page for Edifito.
      class SelectPropertyPage
        URL = 'https://clientes.edifito.com/main.php?prm0=0x1111'

        def initialize(browser)
          @browser = browser
        end

        def open
          @browser.goto(URL) if @browser.url != URL
          as_list_btn_div.click if list_type_span.text != 'Cuadrícula'
          self
        end

        def loaded?
          as_list_btn_div.present?
        end

        def extract_properties
          buildings = {}
          building_selector_divs.wait_until(&:any?)
          building_selector_divs.each do |building_selector_div|
            building_name = building_name_div(building_selector_div).text
            buildings[building_name] = []
            building_selector_div.click
            extract_properties_properties(buildings[building_name])
          end
          buildings
        end

        def select_property(building, property)
          catch(:found) do
            building_selector_divs.each do |building_selector_div|
              next unless building_selector_div.text.include?(building)

              building_selector_div.click
              select_property_property(property)
            end
          end
        end

        protected

        def next_page_loaded?
          @browser.li(class: 'breadcrumb-item').text == 'Dashboard'
        end

        def extract_properties_properties(building)
          property_listing_divs.wait_until(&:any?)
          property_listing_divs.each do |property_listing_div|
            if (r = residente_property(property_listing_div))
              building.push(r)
            else
              extract_inner_properties(building, property_listing_div)
            end
          end
        end

        def extract_inner_properties(building, property_listing_div)
          property_divs(property_listing_div).each do |property_div|
            building.push(property_div.text)
          end
        end

        def residente_property(property_listing_div)
          matches = property_listing_div.text.match(/Residente\s+(\d+)/)
          return matches[1] if matches

          nil
        end

        def select_property_property(property)
          property_listing_divs.wait_until(&:any?)

          property_listing_divs.each do |property_listing_div|
            if (rp = residente_property(property_listing_div))
              actually_select_property(property_listing_div) if rp == property
            else
              select_inner_property(property, property_listing_div)
            end
          end
        end

        def select_inner_property(property, property_listing_div)
          property_divs(property_listing_div).each do |property_div|
            actually_select_property(property_div) if
              property_div.text == property
          end
        end

        def actually_select_property(property_div)
          property_div.click
          @browser.wait_until { next_page_loaded? }
          throw(:found)
        end

        def as_list_btn_div = @browser.div(id: 'tipo_div_com')
        def list_type_span = @browser.span(id: 'tipo_div_com_text')

        def building_selector_divs
          @browser.divs(class: 'seleccionar_comunidad_listado')
        end

        def building_name_div(building_selector_div)
          building_selector_div.div(class: 'span_nombre_comunidad_listado')
        end

        def property_listing_divs
          @browser.divs(class: 'seleccionado_listado_rol', visible: true)
        end

        def property_divs(property_listing_div)
          @browser.execute_script(
            <<~JAVASCRIPT
              let divs = document
                .querySelector('.seleccionado_listado_rol')
                .querySelectorAll('.collapse');

              divs.forEach((el) => { el.classList.add('show'); });
            JAVASCRIPT
          )
          property_listing_div
            .div(class: %w[collapse show])
            .wait_until(&:present?)
          property_listing_div.divs(class: %w[collapse show])
        end
      end
    end
  end
end
