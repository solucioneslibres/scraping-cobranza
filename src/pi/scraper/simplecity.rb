# frozen_string_literal: true

require 'base64'
require 'net/http'
require 'pi/scraper'
require 'pi/scraper/monthly_property_fees'

module PI
  module Scraper
    # Comunidad Feliz API.
    class SimpleCity
      include Credentials

      ENDPOINT = URI('https://simplecity.cl')

      def call(username = nil)
        credentials(:simplecity, username).each do |user|
          login(user[:username], user[:password]).each do |property|
            MonthlyPropertyFees.new(
              'gastos_comunes',
              edificacion: property[:building_id],
              propiedad: property[:property_id],
              **community_fees(property)
            ).store
            puts(
              "simplecity: #{property[:building_id]} " \
              "#{property[:property_id]} - OK"
            )
          end
        end
      end

      protected

      def str_to_date(value)
        Date.strptime(value, '%d/%m/%Y')
      end

      def login(username, password)
        login_request(username, password)[:infoNew].map do |property|
          {
            building_id: property[:b_nombre_edificio],
            property_id: property[:a_sufijo],
            internal_building_id: property[:id_edificio],
            internal_property_id: property[:a_id_detalle_asi]
          }
        end
      end

      def login_request(username, password)
        request(
          '/InicioPrincipal/EntAc/',
          {
            tipo: 1,
            pass: Base64.strict_encode64(password),
            us: username.split('-')[0].gsub('.', ''),
            dispo: 0,
            token: 'nada'
          }
        )
      end

      def community_fees(property)
        community_fees = community_fees_request(property)
        last_boleta = community_fees[:gastos].first
        last_payment = payments_request(property[:internal_property_id]).first
        {
          mes_gc: "#{last_boleta[:a_agno]}-#{last_boleta[:a_mes]}",
          monto_gc: last_boleta[:a_total_mes],
          total_deuda: community_fees[:sumagas].first[:total_pago_gasto],
          fecha_ult_pago: str_to_date(last_payment[:fecha_pago]),
          monto_ult_pago: last_payment[:monto_cancelado]
        }
      end

      def community_fees_request(property)
        request(
          '/Residente/ListaGastosCm/',
          {
            idetasig: property[:internal_property_id],
            idedif: property[:internal_building_id]
          }
        )
      end

      def payments_request(internal_property_id)
        request(
          '/Residente/MuestraPagosWebManual',
          { id_deta: internal_property_id }
        )
      end

      def cookie
        @cookie ||= Net::HTTP.get_response(
          URI("#{ENDPOINT}/InicioPrincipal/Acceso")
        )['Set-Cookie'].split(';')[0]
      end

      def request(path, body)
        Net::HTTP.start(
          ENDPOINT.host,
          ENDPOINT.port,
          use_ssl: ENDPOINT.scheme == 'https'
        ) do |http|
          request = Net::HTTP::Post.new(
            "#{ENDPOINT}#{path}",
            {
              'Content-Type' => 'application/json',
              'Cookie' => cookie
            }
          )
          request.body = body.to_json
          response = http.request(request)
          JSON.parse(response.body, symbolize_names: true)
        end
      end
    end
  end
end

# https://simplecity.cl/InicioPrincipal/ValIni/
# {"id":"21040","tipo":"1","cargo":"residente","dispositivo":"","tipoacceso":"2"}

# https://simplecity.cl/Residente/ListaGastosCm/
# {"idetasig":"21040","idedif":"2220"}

# https://simplecity.cl/Residente/MuestraPagosWebManual
# {"id_deta":"21040"}
