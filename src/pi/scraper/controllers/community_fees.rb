# frozen_string_literal: true

require 'pi/scraper'
require 'pi/scraper/monthly_property_fees'
require 'watir'

module PI
  module Scraper
    module Controllers
      # Community fees scraper controller.
      class CommunityFees
        include Credentials
        include ConvertCaseStyle

        def initialize(browser, platform)
          @browser = browser
          @platform = platform
        end

        def call(
          since_building_id = nil,
          since_property_id = nil,
          username = nil
        )
          credentials(@platform, username).each do |credentials|
            login(credentials)
            select_property_page.open.extract_properties
              .each do |building_id, properties|
                next if since_building_id && since_building_id != building_id

                since_building_id = nil
                process_properties(properties, building_id, since_property_id)
              end
            logout
          end
        end

        def self.spawn_browser
          Watir::Browser.new(:firefox, options: {
            args: ['-headless'],
            prefs: { 'security.OCSP.require' => false }
          })
        end

        protected

        def load_page(page)
          require("pi/scraper/#{@platform}/#{page}")
          PI::Scraper.const_get(
            "#{snake_to_pascal(@platform)}::#{snake_to_pascal(page)}"
          )
        end

        def init_page_args
          return [@browser, @endpoint] if @endpoint

          [@browser]
        end

        def login_page
          load_page('login_page').new(*init_page_args)
        end

        def select_property_page
          load_page('select_property_page').new(*init_page_args)
        end

        def community_fees_page
          load_page('community_fees_page').new(*init_page_args)
        end

        def login(credentials)
          @endpoint = credentials[:endpoint]
          login_page.open.login(
            credentials[:username],
            credentials[:password]
          )
        end

        def logout
          community_fees_page.logout
        end

        def process_properties(properties, building_id, since_property_id)
          properties.each do |property_id|
            next if since_property_id && since_property_id != property_id

            since_property_id = nil
            select_property_page.open.select_property(building_id, property_id)
            extract_monthly_property_fees(building_id, property_id)
          end
        end

        def extract_monthly_property_fees(building_id, property_id)
          MonthlyPropertyFees.new(
            'gastos_comunes',
            edificacion: building_id,
            propiedad: property_id,
            **community_fees_page.open.extract
          ).store
          puts("#{@platform}: #{building_id} #{property_id} - OK")
        end
      end
    end
  end
end
