# frozen_string_literal: true

require 'csv'
require 'json'
require 'pi/scraper'
require 'pi/scraper/boleta_aguas_andinas'
require 'pi/scraper/boleta_enel'
require 'pi/scraper/exceptions/invalid_id_error'
require 'watir'

module PI
  module Scraper
    module Controllers
    # Downloads and extracts data from various Aguas Andinas and Enel accounts.
    #
    # == Example
    #   PI::Scraper::Boletas
    #     .new('source/path.csv')
    #     .process
    #
    # Source path must be a CSV file with headers:
    # - id: property identificator
    # - aandinas_id: Aguas Andinas account number
    # - enel_id: ENEL account number
    class Boletas
      include Path

      def initialize(src_path, company: %i[aandinas enel], since_id: nil)
        src_path = root_path(src_path) unless src_path[0] == '/'
        (warn("#{src_path} not found") && return) unless File.exist?(src_path)

        @src_path = src_path
        @company = company
        @since_id = since_id
      end

      def process
        browser = spawn_browser
        CSV.foreach(@src_path, **CSV_OPTIONS) do |r|
          next if @since_id && r[:id] != @since_id

          @since_id = nil
          process_row(browser, r)
        end
      ensure
        browser.close
      end

      protected

      def spawn_browser
        prepare_ffprofile unless File.exist?('/tmp/ffprofile')
        Watir::Browser.new(:firefox, options: {
          args: ['-profile', '/tmp/ffprofile', '-headless'],
          prefs: {
            'security.OCSP.require' => false,
            'browser.download.folderList' => 2,
            'browser.download.dir' => storage_path('tmp'),
            'browser.download.useDownloadDir' => true
          }
        })
      end

      def prepare_ffprofile # rubocop:disable Metrics/MethodLength
        Dir.mkdir('/tmp/ffprofile')
        pid = Process.spawn(
          'firefox --marionette --headless --profile /tmp/ffprofile'
        )
        Watir::Wait.until { File.exist?('/tmp/ffprofile/handlers.json') }
        Process.kill('TERM', pid)
        File.open('/tmp/ffprofile/handlers.json', 'r+') do |f|
          f.rewind until f.read.include?('application/pdf')
          f.rewind
          handlers = JSON.parse(f.read)
          handlers['mimeTypes']['application/pdf']['action'] = 0
          f.rewind
          f.truncate(0)
          f.write(JSON.generate(handlers))
        end
      end

      def process_row(browser, row)
        {
          aandinas: :BoletaAguasAndinas,
          enel: :BoletaEnel
        }.each do |key, klass|
          if @company.nil? || @company.to_sym == key
            company_id = row["#{key}_id".to_sym]
            PI::Scraper.const_get(klass)
              .new(row[:id], company_id)
              .process(browser)
          end
        rescue InvalidIdError => e
          warn(e.message)
        end
      end
    end
    end
  end
end
