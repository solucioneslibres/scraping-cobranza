# frozen_string_literal: true

module PI
  module Scraper
    # Error thrown when an ID is invalid.
    class InvalidIdError < RuntimeError
    end
  end
end
