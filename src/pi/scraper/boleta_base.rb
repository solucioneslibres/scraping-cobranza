# frozen_string_literal: true

require 'pi/scraper'
require 'watir/wait'

module PI
  module Scraper
    # Base class for boleta. CSV based.
    # Implement: nro_cuenta=, collect(browser), extract
    # Define: ATTRS
    class BoletaBase
      include ConvertCaseStyle
      include CsvStore

      def initialize(id_prop, nro_cuenta)
        @id_prop = id_prop
        self.nro_cuenta = nro_cuenta

        company = snake_case(self.class.name.match(/Boleta(.+)/)[1])
        @pdf_path = storage_path("#{company}/#{Date.today}/#{@nro_cuenta}.pdf")
        @csv_path = storage_path("#{company}/#{Date.today}.csv")
      end

      # @param [Watir::Browser] browser Browser instance to use.
      def process(browser)
        return unless collect(browser)

        extract
        store
      end

      protected

      def move_download
        Watir::Wait.until { tmp_pdf_path }
        FileUtils.mkdir_p(File.dirname(@pdf_path))
        File.rename(tmp_pdf_path, @pdf_path)
      end

      def tmp_pdf_path
        path = storage_path('tmp/*.pdf')
        return nil unless Dir.glob("#{path}.part").empty?

        Dir.glob(path).first
      end
    end
  end
end
