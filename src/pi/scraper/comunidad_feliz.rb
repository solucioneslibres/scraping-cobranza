# frozen_string_literal: true

require 'net/http'
require 'pi/scraper'
require 'pi/scraper/monthly_property_fees'

module PI
  module Scraper
    # Comunidad Feliz API.
    class ComunidadFeliz
      include Credentials

      ENDPOINT = URI('https://app.comunidadfeliz.com/graphql')

      def call(username = nil)
        credentials(:comunidad_feliz, username).each do |user|
          login(user[:username], user[:password])
          extract
          logout
        end
      end

      protected

      def login(username, password)
        response = request <<~GRAPHQL
          mutation {
            signInUser(
              email: {
                email: "#{username}"
                password: "#{password}"
              }
            ) {
              user { token }
            }
          }
        GRAPHQL
        @access_token = response[:data][:signInUser][:user][:token]
        self
      end

      def logout
        @access_token = nil
      end

      def extract
        response = request <<~GRAPHQL
          query {
            user {
              properties {
                name
                communityName
                propertyTransactions(page: 1) {
                  transactionDate
                  transactionValue
                }
                latestBill {
                  pending
                  period
                  billDetails {
                    isPast
                    price
                  }
                }
              }
            }
          }
        GRAPHQL
        response[:data][:user][:properties].each do |property|
          store_property(property)
        end
      end

      def store_property(property) # rubocop:disable Metrics/AbcSize
        last_payment = find_last_payment(property)
        MonthlyPropertyFees.new(
          'gastos_comunes',
          edificacion: property[:communityName],
          propiedad: property[:name],
          mes_gc: format_month(property[:latestBill][:period]),
          monto_gc: month_amount(property[:latestBill][:billDetails]).to_i,
          total_deuda: property[:latestBill][:pending].to_i,
          fecha_ult_pago: (dt_str_to_date(last_payment[:transactionDate]) if
            last_payment),
          monto_ult_pago: (last_payment[:transactionValue].to_i if last_payment)
        ).store
        storage_ok_msg(property)
      end

      def month_amount(bill_details)
        amount = 0
        bill_details.each do |details|
          amount += details[:price] unless details[:isPast]
        end
        amount
      end

      def storage_ok_msg(property)
        puts(
          "comunidad_feliz: #{property[:communityName]} #{property[:name]} - OK"
        )
      end

      def find_last_payment(property)
        property[:propertyTransactions].find do |transaction|
          transaction[:transactionValue].positive?
        end
      end

      def dt_str_to_date(value)
        Date.strptime(value, '%Y-%m-%d')
      end

      def format_month(value)
        month, year = value.split(' - ')
        "#{year}-#{MONTHS_ES.index(month)}"
      end

      def request(query)
        Net::HTTP.start(
          ENDPOINT.host,
          ENDPOINT.port,
          use_ssl: ENDPOINT.scheme == 'https'
        ) do |http|
          headers = { 'Content-Type' => 'application/json' }
          headers['JWTACCESSTOKEN'] = @access_token if @access_token

          request = Net::HTTP::Post.new(ENDPOINT, headers)
          request.body = { query: }.to_json
          response = http.request(request)
          JSON.parse(response.body, symbolize_names: true)
        end
      end
    end
  end
end
