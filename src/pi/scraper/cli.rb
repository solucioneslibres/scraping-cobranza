# frozen_string_literal: true

require 'pi/scraper/comunidad_feliz'
require 'pi/scraper/simplecity'
require 'pi/scraper/controllers/boletas'
require 'pi/scraper/controllers/community_fees'
require 'thor'

module PI
  module Scraper
    # Scraper CLI.
    class CLI < Thor
      PLATFORMS = %w[comunidad_feliz edifito tu_comunidad_en_linea edipro
        simplecity].freeze

      desc(
        'boletas SOURCE_PATH',
        'Descarga boletas y extrae datos de estas. Los almacena en storage/.'
      )
      long_desc <<~LONGDESC
        SOURCE_PATH debe ser un archivo CSV con cabeceras:

        \x5- id: identificador de propiedad
        \x5- aandinas_id: número de cuenta de Aguas Andinas
        \x5- enel_id: número de cuenta de ENEL
      LONGDESC
      option(:desde_id, desc: 'Comenzar desde "id" del CSV')
      option(:empresa, desc: 'Solo "aandinas" o "enel"')
      def boletas(src_path)
        PI::Scraper::Controllers::Boletas.new(
          src_path,
          company: options[:empresa],
          since_id: options[:desde_id]
        ).process
      end

      desc(
        'gastos_comunes',
        'Extrae datos de gastos comunes. Los almacena en storage/.'
      )
      long_desc <<~LONGDESC
        Extrae datos de gastos comunes. Los almacena en storage/.

        La plataforma 'edipro' require de la variable de ambiente
        ANTI_CAPTCHA_API_KEY, correspondiente a un API key válida para
        anti-captcha.com.
      LONGDESC
      option(
        :plataforma,
        desc: "Plataforma: #{PLATFORMS.join(', ')}"
      )
      option(:desde_edificacion, desc: 'Continuar desde edificación')
      option(:desde_propiedad, desc: 'Continuar desde propiedad')
      option(:usuario, desc: 'Usuario de la plataforma')
      def gastos_comunes
        browser = Controllers::CommunityFees.spawn_browser unless
          %w[comunidad_feliz simplecity].include?(options[:plataforma])

        platforms(options[:plataforma]).each do |platform|
          community_fees_per_platform(platform, options, browser)
        end
      ensure
        browser&.close
      end

      def self.exit_on_failure?
        true
      end

      protected

      def community_fees_per_platform(platform, options, browser = nil)
        case platform
        when 'comunidad_feliz'
          ComunidadFeliz.new.call(options[:usuario])
        when 'simplecity'
          SimpleCity.new.call(options[:usuario])
        else
          Controllers::CommunityFees.new(browser, platform).call(
            options[:desde_edificacion],
            options[:desde_propiedad],
            options[:usuario]
          )
        end
      end

      def platforms(platform = nil)
        platform ? [platform] : PLATFORMS
      end
    end
  end
end
