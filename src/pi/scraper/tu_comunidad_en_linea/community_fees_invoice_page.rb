# frozen_string_literal: true

require 'pi/scraper'

module PI
  module Scraper
    class TuComunidadEnLinea
      # Community fees invoice page for TuComunidadEnLinea.
      #
      # Extracts: mes_gc, monto_gc.
      class CommunityFeesInvoicePage
        include MoneyToInt

        URL = 'https://tcel.cl/web/boletas'

        def initialize(browser)
          @browser = browser
        end

        def open
          @browser.goto(URL)
          @browser.wait_until{ month_amount_cell.visible? }
          self
        end

        def extract
          {
            mes_gc: "#{year_select.value}-#{month_select.value}",
            monto_gc: money_to_int(month_amount_cell.text)
          }
        end

        protected

        def year_select = @browser.select(name: 'anio')
        def month_select = @browser.select(name: 'mes')

        def month_amount_cell
          @browser.table(index: -1).row(index: 1).cell(index: -1)
        end
      end
    end
  end
end
