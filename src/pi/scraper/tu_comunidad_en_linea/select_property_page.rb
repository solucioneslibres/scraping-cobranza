# frozen_string_literal: true

require 'pi/scraper/tu_comunidad_en_linea/community_fees_page'

module PI
  module Scraper
    class TuComunidadEnLinea
      # Select property page for TuComunidadEnLinea.
      class SelectPropertyPage
        URL = 'https://tcel.cl/web/prehome'

        def initialize(browser)
          @browser = browser
        end

        def open
          @browser.goto(URL) if @browser.url != URL
          self
        end

        def loaded?
          property_divs.any?
        end

        def extract_properties
          buildings = {}
          property_divs.each do |property_div|
            building = property_div.p(index: 1).text
            property = property_div.h4.text
            buildings[building] ||= []
            buildings[building].push(property)
          end
          buildings
        end

        def select_property(building_id, property_id)
          property_divs.each do |property_div|
            next unless property_div.p(index: 1).text == building_id &&
                        property_div.h4.text == property_id

            property_div.click
            next_page = CommunityFeesPage.new(@browser)
            @browser.wait_until { next_page.loaded? }
            break
          end
        end

        protected

        def property_divs = @browser.divs(class: 'col-sm-4')
      end
    end
  end
end
