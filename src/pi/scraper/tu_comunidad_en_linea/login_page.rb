# frozen_string_literal: true

require 'pi/scraper/tu_comunidad_en_linea/select_property_page'

module PI
  module Scraper
    class TuComunidadEnLinea
      # Login page for TuComunidadEnLinea.
      class LoginPage
        URL = 'https://tcel.cl'

        def initialize(browser)
          @browser = browser
        end

        def open
          @browser.goto(URL)
          self
        end

        def login(user, password)
          user_field.set(user)
          enter_button.click

          password_field.set(password)
          enter_button.click

          next_page = SelectPropertyPage.new(@browser)
          Watir::Wait.until { next_page.loaded? }
        end

        protected

        def user_field = @browser.text_field(id: 'email')
        def password_field = @browser.text_field(id: 'password')
        def enter_button = @browser.button(class: 'btn')
      end
    end
  end
end
