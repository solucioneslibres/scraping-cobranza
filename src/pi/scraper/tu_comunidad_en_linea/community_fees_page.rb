# frozen_string_literal: true

require 'pi/scraper'
require 'pi/scraper/tu_comunidad_en_linea/community_fees_invoice_page'

module PI
  module Scraper
    class TuComunidadEnLinea
      # Community fees page for TuComunidadEnLinea.
      #
      # Extracts: total_deuda, fecha_ult_pago, monto_ult_pago +
      # CommunityFeesInvoicePage#extract.
      class CommunityFeesPage
        include MoneyToInt

        URL = 'https://tcel.cl/web/home'

        def initialize(browser)
          @browser = browser
        end

        def open
          @browser.goto(URL) if @browser.url != URL
          self
        end

        def loaded?
          debt_amount_h3.present?
        end

        def extract
          {
            total_deuda: money_to_int(debt_amount_h3.text),
            fecha_ult_pago: to_date(last_payment_date_div.text).to_s,
            monto_ult_pago: money_to_int(last_payment_amount_div.text)
          }.merge(CommunityFeesInvoicePage.new(@browser).open.extract)
        end

        def logout
          @browser.goto('https://tcel.cl/web/prehome/cerrar_sesion')
        end

        protected

        def to_date(value)
          Date.strptime(value, '%d/%m/%Y')
        end

        def debt_amount_h3 = @browser.h3(index: 1)

        def summary_div
          @browser.div(class: 'mt-3', index: 3).div(class: 'card')
        end

        def last_payment_date_div
          summary_div.div(class: 'row', index: 0).div(index: -1)
        end

        def last_payment_amount_div
          summary_div.div(class: 'row', index: 1).div(index: -1)
        end
      end
    end
  end
end
