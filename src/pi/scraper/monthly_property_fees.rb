# frozen_string_literal: true

require 'pi/scraper'

module PI
  module Scraper
    # Monthly community fees for a property.
    class MonthlyPropertyFees
      include CsvStore
      include Path

      ATTRS = %w[edificacion propiedad mes_gc monto_gc total_deuda
        fecha_ult_pago monto_ult_pago].freeze

      def initialize(path, **args)
        @csv_path = storage_path("#{path}/#{Date.today}.csv")

        args.each { |k, v| instance_variable_set("@#{k}", v) }
      end
    end
  end
end
