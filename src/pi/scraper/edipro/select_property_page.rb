# frozen_string_literal: true

module PI
module Scraper
module Edipro
# Select property page for Edipro.
class SelectPropertyPage
  def initialize(browser, endpoint)
    @browser = browser
    @endpoint = endpoint
  end

  def open
    @browser.goto("#{@endpoint}/mis_departamentos")
    self
  end

  def extract_properties
    buildings = {}
    buildings[building_id_span.text] = []
    property_id_links.each do |property_id_link|
      buildings[building_id_span.text].push(property_id_link.text)
    end
    buildings
  end

  def select_property(_building_id, property_id)
    property_id_links.find { |pid_link| pid_link.text == property_id }.click
    @browser.wait_until { next_page_loaded? }
  end

  protected

  def next_page_loaded?
    @browser.h2(class: 'main-content-title').text.include?('Departamento')
  end

  def building_id_span = @browser.span(class: 'nav-label')
  def property_id_links = @browser.links(class: 'custom-box-btn')
end
end
end
end
