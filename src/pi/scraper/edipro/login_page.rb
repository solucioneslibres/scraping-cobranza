# frozen_string_literal: true

# require 'api_2captcha'

require 'anti_captcha'
require 'watir/wait'

module PI
module Scraper
module Edipro
# Login page for Edipro.
class LoginPage
  def initialize(browser, endpoint)
    @browser = browser
    @url = "#{endpoint}/auth/users/sign_in"
  end

  def open
    @browser.goto(@url)
    self
  end

  def login(user, password) # rubocop:disable Metrics/MethodLength
    retries = 0
    begin
      user_field.set(user)
      password_field.set(password)
      anti_captcha_task_id = bypass_recaptcha
      @browser.send_keys(:enter)
      @browser.wait_until(timeout: 10) { next_page_loaded? }
      anti_captcha.report_correct_recaptcha(anti_captcha_task_id)
    rescue Watir::Wait::TimeoutError => e
      anti_captcha.report_incorrect_recaptcha(anti_captcha_task_id)
      raise(e) unless retries < 10

      retries += 1
      warn("(#{retries}) invalid captcha for ID: #{anti_captcha_task_id}")
      retry
    end
  end

  protected

  def anti_captcha
    @anti_captcha ||= AntiCaptcha.new(ENV.fetch('ANTI_CAPTCHA_API_KEY'))
  end

  def bypass_recaptcha
    task_id = anti_captcha.create_task({
      type: 'RecaptchaV3TaskProxyless',
      websiteURL: @url,
      websiteKey: '6Lf0M58aAAAAACAnuLvb5wvzjlgtTqXFRvr8cZC3',
      minScore: 0.9,
      pageAction: 'login',
      isEnterprise: false
    })[:taskId]
    result = anti_captcha.get_task_result(task_id)
    @browser.execute_script(
      'document.querySelector(".token-recaptcha").value = ' \
      "'#{result[:solution][:gRecaptchaResponse]}'"
    )
    task_id
  end

  def next_page_loaded?
    @browser.h2(class: 'main-content-title').visible?
  end

  def user_field = @browser.text_field(id: 'user_login')
  def password_field = @browser.text_field(id: 'user_password')
end
end
end
end
