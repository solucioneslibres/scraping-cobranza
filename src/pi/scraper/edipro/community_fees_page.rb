# frozen_string_literal: true

require 'pi/scraper'

module PI
module Scraper
module Edipro
# Community fees page for Edipro.
#
# Extracts: mes_gc, monto_gc, total_deuda, fecha_ult_pago, monto_ult_pago.
class CommunityFeesPage
  include MoneyToInt

  def initialize(browser, endpoint)
    @browser = browser
    @endpoint = endpoint
  end

  def open
    self
  end

  def extract
    {
      mes_gc: parse_month(month_cell.text),
      monto_gc: money_to_int(month_amount_cell.text) +
        money_to_int(month_amount_cell2.text),
      total_deuda: money_to_int(debt_amount_li.text.split(':')[1]),
      fecha_ult_pago: str_to_date(last_payment_date_cell.text),
      monto_ult_pago: money_to_int(last_payment_amount_cell.text)
    }
  end

  def logout
    @browser.goto("#{@endpoint}/auth/users/sign_out")
  end

  protected

  def parse_month(value)
    month_es, year = value.split('/')
    format(
      '%<year>04d-%<month>02d',
      { year:, month: MONTHS_ES.index(month_es.downcase) }
    )
  end

  def str_to_date(value)
    Date.strptime(value, '%d/%m/%Y')
  end

  def tabs_div = @browser.div(id: 'contabilidad-tab')

  def ingresos_table
    tabs_div.button(index: 0).click
    selector = @browser.table
    selector.wait_until(&:present?)
    selector
  end

  def colillas_table
    tabs_div.button(index: 1).click
    selector = @browser.table(index: 1)
    selector.wait_until(&:present?)
    selector
  end

  def debt_amount_li = @browser.ul(class: 'no-bullets', index: 1).li(index: -2)

  def month_cell = colillas_table.tbody.row(index: 0).cell(index: 0)
  def month_amount_cell = colillas_table.tbody.row(index: 0).cell(index: 1)
  def month_amount_cell2 = colillas_table.tbody.row(index: 0).cell(index: 2)
  def last_payment_date_cell = ingresos_table.tbody.row(index: 0).cell(index: 3)

  def last_payment_amount_cell
    ingresos_table.tbody.row(index: 0).cell(index: 1)
  end
end
end
end
end
