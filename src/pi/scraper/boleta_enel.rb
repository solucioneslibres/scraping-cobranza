# frozen_string_literal: true

require 'csv'
require 'date'
require 'fileutils'
require 'faker'
require 'monkeys/pdf_reader'
require 'pi/scraper'
require 'pi/scraper/boleta_base'
require 'pi/scraper/exceptions/invalid_id_error'

module PI
  module Scraper
    # Downloads boleta and extracts data for an Enel account.
    #
    # All is stored on storage/.
    #
    # == Example
    #   PI::Scraper::BoletaEnel
    #     .new(id_prop, nro_cuenta)
    #     .process(browser)
    class BoletaEnel < BoletaBase
      include Path
      include MoneyToInt

      ATTRS = %w[
        id_prop nro_cuenta lectura_anterior saldo_anterior lectura_actual
        total_a_pagar
      ].freeze

      # Automate browser to download PDF.
      # @param [Watir::Browser] browser Browser instance to use.
      def collect(browser)
        browser.goto(
          'https://www.enel.cl/es/clientes/servicios-en-linea/copia-boleta.html'
        )

        browser.text_field(id: 'rutStep1').set('11111111-1')
        browser.send_keys(:enter)

        numero_cliente_field = browser.text_field(id: 'numero_cliente')
        numero_cliente_field.set(@nro_cuenta.sub('-', ''))
        browser.send_keys(:enter)

        if numero_cliente_field.class_name.include?('error')
          warn("Cuenta '#{@nro_cuenta}' inválido para Enel (#{@id_prop})")
          return
        end
        browser.button(id: 'button-step2').click

        loader_div = browser.div(class: 'loader-overlay')
        loader_div.wait_until { |el| !el.exists? }

        browser.img(id: 'truste-consent-close').click # close cookies footer

        browser.text_field(id: 'nombreCliente').set(Faker::Name.first_name)
        browser.text_field(id: 'apellidoCliente').set(Faker::Name.last_name)
        browser.text_field(id: 'telefonoCliente')
          .set(rand((910_000_000..999_999_999)))
        browser.text_field(id: 'correoCliente').set(Faker::Internet.email)
        browser.execute_script(
          'document.querySelector("#ckbxConsentAgeMsg").click()'
        )
        browser.execute_script(
          'document.querySelector("label[for=politica_privacidad0]").click()'
        )
        browser.button(id: 'ver_boleta').click
        browser.send_keys(:enter)

        if browser.div(id: 'mensajeError').present?
          warn(
            "Boleta para cuenta '#{@nro_cuenta}' temporalmente no disponible " \
            "en Enel (#{@id_prop})"
          )
          return
        end

        download_button = browser.button(id: 'downloadBoleta')
        download_button.wait_until(&:present?)
        loader_div.wait_until { |el| !el.exists? }
        download_button.click
        move_download

        puts("enel: #{@nro_cuenta} (#{@id_prop}) - OK")
        true
      end

      # Extract data from PDF.
      def extract
        pages = PDF::Reader.new(@pdf_path).pages
        type = pages.first.text.match(/(BOLETA|FACTURA) ELECTR[OÓ]NICA/)[1]

        type == 'FACTURA' ? extract_factura(pages) : extract_boleta(pages)
      end

      protected

      def nro_cuenta=(value)
        @nro_cuenta = value&.strip
        return unless @nro_cuenta&.length != 9 || @nro_cuenta[-2] != '-'

        raise(
          InvalidIdError,
          "Cuenta '#{@nro_cuenta}' inválida para Enel (#{@id_prop})"
        )
      end

      def text_to_date(value)
        Date.parse(value, '%d/%m/%Y')
      end

      def extract_boleta(pages)
        text = pages[1].text(unformatted: true)
        periods_matches = text.match(%r{Per[ií]odo de lectura[:\s]+([^-]+)[-\s]+([\d/]+)})
        @lectura_anterior = text_to_date(periods_matches[1]) - 1
        @lectura_actual = text_to_date(periods_matches[2])
        @saldo_anterior = money_to_int(
          text.match(/Saldo anterior\s+\$\s+([-\d.]+)/)[1]
        )
        @total_a_pagar = money_to_int(
          text.match(/Total a pagar\s+[^\d]+(En Crédito|[\d.]+)/)[1]
        )
      end

      def extract_factura(pages)
        @lectura_anterior = text_to_date(
          pages.first.text(rect: PDF::Reader::Rectangle.new(420, 300, 465, 315))
        ) - 1 # assuming previous period corresponds to one day less
        @saldo_anterior = '?'
        @lectura_actual = text_to_date(
          pages.first.text(rect: PDF::Reader::Rectangle.new(520, 300, 565, 315))
        )
        @total_a_pagar = money_to_int(
          pages.first.text(rect: PDF::Reader::Rectangle.new(610, 180, 770, 220))
        )
      end
    end
  end
end
