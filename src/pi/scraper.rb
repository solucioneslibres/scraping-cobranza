# frozen_string_literal: true

require 'fileutils'
require 'net/http'

module PI
  # PI::Scraper collects and stores billing data from Aguas Andinas, Enel and
  # various property management portals.
  module Scraper
    CSV_OPTIONS = { headers: true, header_converters: :symbol }.freeze

    MONTHS_ES = %w[cero enero febrero marzo abril mayo junio julio agosto
      septiembre octubre noviembre diciembre].freeze

    # Common methods for paths.
    module Path
      protected

      def storage_path(path = nil)
        return root_path("storage/#{path}") if path

        root_path('storage')
      end

      def root_path(path = nil)
        return "#{Dir.pwd}/#{path}" if path

        Dir.pwd
      end
    end

    # Store a model class to a CSV file.
    #
    # Define: ATTRS, @csv_path
    module CsvStore
      # Store this object on a CSV file.
      def store
        csv_prepend_headers unless File.exist?(@csv_path)

        CSV.open(@csv_path, 'a', **CSV_OPTIONS) do |csv|
          csv << self.class.const_get(:ATTRS).map do |attr|
            instance_variable_get("@#{attr}")
          end
        end
      end

      protected

      def csv_prepend_headers
        FileUtils.mkdir_p(File.dirname(@csv_path))
        File.write(@csv_path, "#{self.class.const_get(:ATTRS).join(',')}\n")
      end
    end

    # Convert a string from PascalCase to snake_case and vice-versa.
    module ConvertCaseStyle
      protected

      def snake_case(value)
        value.gsub(/(.)([A-Z])/, '\1_\2').downcase
      end

      def snake_to_pascal(value)
        value.split(/-|_/).map(&:capitalize).join
      end
    end

    # Convert from money string to int with money_to_int method.
    module MoneyToInt
      protected

      def money_to_int(value)
        value.gsub(/[$ .]/, '').to_i
      end
    end

    # Get credentials with credentials method.
    module Credentials
      include Path

      protected

      def credentials(platform, username = nil)
        require(root_path('credentials.rb'))
        return CREDENTIALS[platform.to_sym] if username.nil?

        CREDENTIALS[platform.to_sym].select { |c| username == c[:username] }
      end
    end
  end
end
