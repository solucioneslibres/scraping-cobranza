# frozen_string_literal: true

CREDENTIALS = {
  comunidad_feliz: [
    {
      username: 'user@domain.example',
      password: 'password'
    }
  ],
  edifito: [
    {
      username: 'user@domain.example',
      password: 'password'
    }
  ],
  tu_comunidad_en_linea: [
    {
      username: 'user@domain.example',
      password: 'password'
    }
  ],
  edipro: [
    {
      endpoint: 'https://endpoint.edipro.cl',
      username: 'user@domain.example',
      password: 'password'
    }
  ],
  simplecity: [
    {
      username: '11.111.111-1',
      password: 'password'
    }
  ]
}.freeze
